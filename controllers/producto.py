# -*- coding: utf-8 -*-
def index():
    rows = db().select(db.producto.ALL, orderby=db.producto.descripcion)
    return dict(rows=rows)

def crear():
    if request.post_vars:
        #insertar
        db.producto.insert(descripcion = request.post_vars.descripcion, precio = request.post_vars.precio)
        redirect(URL('producto', 'index'))
    return dict()

def editar():
    producto_id = request.args(0)
    row_producto = db(db.producto.id == producto_id).select(db.producto.ALL).first()

    if request.post_vars:
        #Actualizar
        db(db.producto.id == producto_id).update(descripcion = request.post_vars.descripcion, precio = request.post_vars.precio)
        redirect(URL('producto', 'index'))

    return dict(row_producto=row_producto)

def eliminar():
    producto_id = request.post_vars.producto_id
    if db(db.producto.id == producto_id).delete() == 1:
        redirect(URL('producto', 'index'))
        return True
    return False
