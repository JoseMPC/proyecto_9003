# -*- coding: utf-8 -*-
def index():
    rows = db().select(db.pedido.ALL, orderby=db.pedido.viaje_id)
    return dict(rows=rows)

def crear():
    rows_viaje = db().select(db.viaje.ALL, orderby=db.viaje.persona_id)
    rows_persona = db().select(db.persona.ALL, orderby=db.persona.nombre)
    if request.post_vars:
        #insertar
        db.pedido.insert(viaje_id = request.post_vars.viaje_id, persona_id = request.post_vars.persona_id, monto = request.post_vars.monto, comision = request.post_vars.comision)
        redirect(URL('pedido', 'index'))
    return dict(rows_viaje=rows_viaje, rows_persona=rows_persona)

def editar():
    pedido_id = request.args(0)
    row_pedido = db(db.pedido.id == pedido_id).select(db.pedido.ALL).first()
    rows_viaje = db().select(db.viaje.ALL, orderby=db.viaje.persona_id)
    rows_persona = db().select(db.persona.ALL, orderby=db.persona.nombre)

    if request.post_vars:
        #Actualizar
        db(db.pedido.id == pedido_id).update(viaje_id = request.post_vars.viaje_id, persona_id = request.post_vars.persona_id, monto = request.post_vars.monto, comision = request.post_vars.comision)
        redirect(URL('pedido', 'index'))

    return dict(row_pedido=row_pedido, rows_viaje=rows_viaje, rows_persona=rows_persona)

def eliminar():
    pedido_id = request.post_vars.pedido_id
    if db(db.pedido.id == pedido_id).delete() == 1:
        redirect(URL('pedido', 'index'))
        return True
    return False
