# -*- coding: utf-8 -*-
def index():
    rows = db().select(db.detalle_pedido.ALL)#, orderby=db.detalle_pedido.pedido_id.viaje_id.persona_id.nombre)
    return dict(rows=rows)

def crear():
    rows_pedido = db().select(db.pedido.ALL)
    rows_producto = db().select(db.producto.ALL, orderby=db.producto.descripcion)
    if request.post_vars:
        #insertar
        db.detalle_pedido.insert(pedido_id = request.post_vars.pedido_id, producto_id = request.post_vars.producto_id, cantidad = request.post_vars.cantidad,
                                 costo = request.post_vars.costo, comprado = request.post_vars.comprado == 'si')
        redirect(URL('detalle_pedido', 'index'))
    return dict(rows_pedido=rows_pedido, rows_producto=rows_producto)

def editar():
    detalle_pedido_id = request.args(0)
    row_detalle_pedido = db(db.detalle_pedido.id == detalle_pedido_id).select(db.detalle_pedido.ALL).first()
    rows_pedido = db().select(db.pedido.ALL)
    rows_producto = db().select(db.producto.ALL, orderby=db.producto.descripcion)

    if request.post_vars:
        #Actualizar
        db(db.detalle_pedido.id == detalle_pedido_id).update(pedido_id = request.post_vars.pedido_id, producto_id = request.post_vars.producto_id, cantidad = request.post_vars.cantidad,
                                                             costo = request.post_vars.costo, comprado = request.post_vars.comprado == 'si')
        redirect(URL('detalle_pedido', 'index'))
    return dict(row_detalle_pedido=row_detalle_pedido, rows_pedido=rows_pedido, rows_producto=rows_producto)

def eliminar():
    detalle_pedido_id = request.post_vars.detalle_pedido_id
    if db(db.detalle_pedido.id == detalle_pedido_id).delete() == 1:
        redirect(URL('detalle_pedido', 'index'))
        return True
    return False

def calcularCosto():
    producto_id = request.post_vars.producto_id
    cantidad = int(request.post_vars.cantidad)
    row = db(db.producto.id == producto_id).select(db.producto.precio).first()
    importe = cantidad * row.precio
    return ' {"costo" : "' + str(importe) + '" } '
