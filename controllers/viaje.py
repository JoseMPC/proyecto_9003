# -*- coding: utf-8 -*-
def index():
    rows = db().select(db.viaje.ALL)
    return dict(rows=rows)

def crear():
    rows_persona = db().select(db.persona.ALL, orderby=db.persona.nombre)
    if request.post_vars:
        #insertar
        db.viaje.insert(persona_id = request.post_vars.persona_id, fecha = request.now)
        redirect(URL('viaje', 'index'))
    return dict(rows_persona=rows_persona)

def editar():
    viaje_id = request.args(0)
    row_viaje = db(db.viaje.id == viaje_id).select(db.viaje.ALL).first()
    rows_persona = db().select(db.persona.ALL, orderby=db.persona.nombre)

    if request.post_vars:
        #Actualizar
        db(db.viaje.id == viaje_id).update(persona_id = request.post_vars.persona_id, fecha = request.now)
        redirect(URL('viaje', 'index'))
    return dict(row_viaje=row_viaje, rows_persona=rows_persona)

def eliminar():
    viaje_id = request.post_vars.viaje_id
    if db(db.viaje.id == viaje_id).delete() == 1:
        redirect(URL('viaje', 'index'))
        return True
    return False
