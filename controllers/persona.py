# -*- coding: utf-8 -*-
def index():
    rows = db().select(db.persona.ALL, orderby=db.persona.nombre)
    return dict(rows=rows)

def crear():
    if request.post_vars:
        #insertar
        db.persona.insert(nombre = request.post_vars.nombre)
        redirect(URL('persona', 'index'))
    return dict()

def editar():
    persona_id = request.args(0)
    #row_persona = db.persona(persona_id)
    row_persona = db(db.persona.id == persona_id).select(db.persona.ALL).first()

    if request.post_vars:
        #Actualizar
        db(db.persona.id == persona_id).update(nombre = request.post_vars.nombre)
        redirect(URL('persona', 'index'))

    return dict(row_persona=row_persona)

def eliminar():
    persona_id = request.post_vars.persona_id
    if db(db.persona.id == persona_id).delete() == 1:
        redirect(URL('persona', 'index'))
        return True
    return False
